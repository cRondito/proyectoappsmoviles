﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoDestroy : MonoBehaviour
{
    [SerializeField] public string objectToDestroy;

    void Start()
    {
        Destroy(GameObject.Find(objectToDestroy));
    }

}
