﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BallLauncher : MonoBehaviour
{
    private Vector3 startDragPosition;
    private Vector3 endDragPosition;
    private LaunchPreview launchPreview;
    private BlockSpawner blockSpawner;
    private int ballsTotal = 0;
    private int ballsReturned;
    private bool flag = true;
    
    [SerializeField] private Ball ballPrefab = null;
    [SerializeField] TextMeshProUGUI scoreText = null;

    private void Awake()
    {
        blockSpawner = FindObjectOfType<BlockSpawner>();
        launchPreview = GetComponent<LaunchPreview>();
        scoreText.text = "LEVEL 0";
        launchPreview.lineRenderer.enabled = false;
        CreateBall();
    }

    public void ReturnBall()
    {
        ballsReturned++;
        if (ballsReturned == ballsTotal)
        {
            blockSpawner.SpawnRowOfBlocks();
            CreateBall();
            
            ballsReturned = 0;
            flag = true;
        }
    }

    private void CreateBall()
    {
        ballsTotal++;
    }

    private void Update()
    {
        scoreText.text = "LEVEL " + ballsTotal;

        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) + Vector3.back * -10;

        if (Input.GetMouseButtonDown(0) && flag == true)
        {
            StartDrag(worldPosition);
            launchPreview.lineRenderer.enabled = true;
        }
        else if (Input.GetMouseButton(0) && flag == true)
        {
            ContinueDrag(worldPosition);
        }
        else if (Input.GetMouseButtonUp(0) && flag == true)
        {
            EndDrag();
            launchPreview.lineRenderer.enabled = false;
        }

        DataHolder.Score = ballsTotal;
    }

    private void EndDrag()
    {
        StartCoroutine(LaunchBalls());
        flag = false;
    }

    private IEnumerator LaunchBalls()
    {
        Vector3 direction = Vector3.down;
        direction.Normalize();
        if (endDragPosition != startDragPosition)
        {
            direction = endDragPosition - startDragPosition;
            direction.Normalize();
        }
        

        //foreach (var ball in balls)
        for(int i = 0; i < ballsTotal ;i++)
        {
            var ball = Instantiate(ballPrefab);
            ball.transform.position = transform.position;
            ball.GetComponent<Rigidbody2D>().AddForce(-direction);

            yield return new WaitForSeconds(0.08f);
        }

    }

    private void ContinueDrag(Vector3 worldPosition)
    {
        endDragPosition = worldPosition;
        Vector3 direction = endDragPosition - startDragPosition;
        launchPreview.SetEndPoint(transform.position - direction);
    }

    private void StartDrag(Vector3 worldPosition)
    {
        startDragPosition = worldPosition;
        launchPreview.SetStartPoint(transform.position);
    }

    public int GetScore()
    {
        int score = ballsTotal;
        return score;
    }
}
