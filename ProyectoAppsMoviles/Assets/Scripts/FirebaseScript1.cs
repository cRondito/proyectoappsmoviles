﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class FirebaseScript1 : MonoBehaviour
{
    //DatabaseReference reference;
    [SerializeField] TextMeshProUGUI leaderboardText1;
    [SerializeField] TextMeshProUGUI leaderboardText2;
    [SerializeField] TextMeshProUGUI leaderboardText3;
    [SerializeField] TextMeshProUGUI leaderboardText4;
    [SerializeField] TextMeshProUGUI leaderboardText5;
    List<string> list = new List<string>(10);
    private float nextActionTime = 0.0f;
    public float period = 0.3f;
    string aux;

    void Start()
    {
        //reference = FirebaseDatabase.DefaultInstance.RootReference;
        leaderboardText1.text = "";
        leaderboardText2.text = "";
        leaderboardText3.text = "loading...";
        leaderboardText4.text = "";
        leaderboardText5.text = "";
        list.Add("0");

        retrieveData();

    }

    private void Update()
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            imprimirLista(list);
        }
    }

    void retrieveData()
    {
        FirebaseDatabase.DefaultInstance.GetReference("Leaders").OrderByChild("score").GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                print("Error");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                foreach (var snap in snapshot.Children)
                {
                    var name = snap.Child("name").Value.ToString();
                    var score = snap.Child("score").Value;
                    aux = name.ToString() + "" + score.ToString();
                    list.Add(aux);
                    //Debug.Log(aux);
                }
                // Do something with snapshot...
            }
     });

        /*Firebase.Database.FirebaseDatabase dbInstance = Firebase.Database.FirebaseDatabase.DefaultInstance;
        dbInstance.GetReference("Leaders").OrderByChild("score").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                print("Error");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                foreach (DataSnapshot user in snapshot.Children)
                {
                    IDictionary dictUser = (IDictionary)user.Value;
                    aux = ("" + dictUser["name"] + " - " + dictUser["score"]);
                    Debug.Log(aux);
                    
                }
                
            }

        });*/

    }

    void imprimirLista(List<string> list)
    {
        
        leaderboardText1.text = "1. " + list[1];
        leaderboardText2.text = "2. " + list[2];
        leaderboardText3.text = "3. " + list[3];
        leaderboardText4.text = "4. " + list[4];
        leaderboardText5.text = "5. " + list[5];
    }
    
}
