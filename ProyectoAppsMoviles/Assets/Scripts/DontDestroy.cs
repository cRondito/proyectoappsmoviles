﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;

public class DontDestroy : MonoBehaviour
{
    AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(transform.gameObject);
        audioSource.Play();

    }

    private void Update()
    {

        if (SceneManager.GetActiveScene().name == "EndScene")
        {
            audioSource.Stop();
        }
    }

}
