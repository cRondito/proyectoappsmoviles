﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{

    public static AudioClip blockSound, breakBlock;
    static AudioSource audioSrc;

    void Start()
    {
        blockSound = Resources.Load<AudioClip>("BlockSound");
        breakBlock = Resources.Load<AudioClip>("BreakBlock");
        audioSrc = GetComponent<AudioSource>();
    }

    void Update()
    {
        
    }

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "block":
                audioSrc.PlayOneShot(blockSound);
                break;
            case "break":
                audioSrc.PlayOneShot(breakBlock);
                break;
        }
    }
}
