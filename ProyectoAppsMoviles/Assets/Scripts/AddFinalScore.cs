﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AddFinalScore : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI scoreText = null;

    void Start()
    {
        scoreText.text = "Final Score: " + DataHolder.Score;
    }

    
}
