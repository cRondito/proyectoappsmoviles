﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FirebaseScript : MonoBehaviour
{
    DatabaseReference reference;
    [SerializeField] TextMeshProUGUI sentText = null;
    [SerializeField] Button button = null;

    void Start()
    {
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        button.onClick.AddListener(() => sendToDatabase());
    }

    public void sendToDatabase()
    {
        string leaders = "Leaders";
        string name = DataHolder.Username;
        int points = DataHolder.Score;
        int id = Random.Range(1,1000);
        if (name != "")
        {
            string DebugMsg = saveDataToFirebase(leaders, id, name, points);
            sentText.gameObject.SetActive(true);
            button.enabled = false;
            //Debug.Log(DebugMsg); // print message in console
        }
    }

    public string saveDataToFirebase(string leaders, float id, string name, int score)
    {
        reference.Child(leaders.ToString()).Child(id.ToString()).Child("name").SetValueAsync(name);
        reference.Child(leaders.ToString()).Child(id.ToString()).Child("score").SetValueAsync(-score);
        return "Save data to firebase Done.";
    }
}