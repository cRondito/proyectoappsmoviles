﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSceneChanger : MonoBehaviour
{
    [SerializeField] public string scene;

    void Start()
    {
        SceneManager.LoadScene(scene);
    }

}
