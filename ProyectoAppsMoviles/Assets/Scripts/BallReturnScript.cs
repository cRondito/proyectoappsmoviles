﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallReturnScript : MonoBehaviour
{
    private BallLauncher ballLauncher;

    private void Awake()
    {
        ballLauncher = FindObjectOfType<BallLauncher>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ballLauncher.ReturnBall();
        Destroy(collision.collider.gameObject);

        Vector3 linePos = collision.transform.position;
        float linePosX = collision.transform.position.x;
        if (linePosX < 2.40 && linePosX > -2.40)
        {
            Vector3 linePos2 = new Vector3(linePosX, -2.78f, 0.0f);
            ballLauncher.transform.position = linePos2;
        }
    }
}