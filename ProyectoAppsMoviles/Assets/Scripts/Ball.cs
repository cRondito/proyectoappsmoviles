﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private new Rigidbody2D rigidbody2D;
    public float timer = 0.0f;
    private Vector2 lastVelocity;

    [SerializeField]
    private float moveSpeed = 20;

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        lastVelocity = rigidbody2D.velocity;
    }

    private void Update()
    {
        rigidbody2D.velocity = rigidbody2D.velocity.normalized * moveSpeed;
        
        timer += Time.deltaTime;
        if (timer > 10.0f)
        {
            moveSpeed = 30;
            rigidbody2D.gravityScale = 0.1f;
        }
        
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 surfaceNormal = collision.contacts[0].normal;
        rigidbody2D.velocity = Vector2.Reflect(lastVelocity, surfaceNormal);
        
    }

}
