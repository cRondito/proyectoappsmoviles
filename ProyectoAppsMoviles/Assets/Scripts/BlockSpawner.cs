﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    [SerializeField]
    private Block blockPrefab = null;

    private int playWidth = 7;
    private float distanceBetweenBlocks = 0.72f;
    private int rowsSpawned;

    private List<Block> blockSpawned = new List<Block>();

    private void OnEnable()
    {
        for (int i = 0; i < 1; i++)
        {
            SpawnRowOfBlocks();
        }
    }

    public void SpawnRowOfBlocks()
    {
        foreach(var block in blockSpawned)
        {
            if(block != null)
            {
                block.transform.position += Vector3.down * distanceBetweenBlocks;
            }
        }

        int randNum = UnityEngine.Random.Range(0, 6);

        for (int i = 0; i < playWidth; i++)
        {
            if (i == randNum)
            {
                var block = Instantiate(blockPrefab, GetPosition(i), Quaternion.identity);
                int hits = UnityEngine.Random.Range(3, 6) + rowsSpawned;

                block.SetHits(hits);

                blockSpawned.Add(block);
            }
            else if (UnityEngine.Random.Range(0,100) <= 40)
            {
                var block = Instantiate(blockPrefab, GetPosition(i), Quaternion.identity);
                int hits = UnityEngine.Random.Range(3, 6) + rowsSpawned;

                block.SetHits(hits);

                blockSpawned.Add(block);
            }
            
        }

        rowsSpawned++;
    }

    private Vector3 GetPosition(int i)
    {
        Vector3 position = transform.position;
        position += Vector3.right * i * distanceBetweenBlocks;
        return position;
    }
}
