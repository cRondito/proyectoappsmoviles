﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHolder : MonoBehaviour
{
    public static int Score { get; set; }
    public static string Username { get; set; }
}
